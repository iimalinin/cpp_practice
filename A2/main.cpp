#include <iostream>
#include <cassert>
#include <cstddef>

#include "rectangle.hpp"
#include "circle.hpp"
#include "polygon.hpp"


void showRectangle(tchervinsky::rectangle_t rect)
{
  std::cout << "width: " << rect.width << ", height: " << rect.height;
  std::cout << ", x: " << rect.pos.x << ", y: " << rect.pos.y;
  std::cout << std::endl;
}

void showArea(const tchervinsky::Shape *shape)
{
  assert(shape != nullptr && "shape parameter is invalid, nullptr");
  std::cout << "Area: " << shape->getArea() << std::endl;
}

int main()
{
  tchervinsky::Rectangle rect(10.0, 5.0, {15.0, 15.0});
  showArea(&rect);
  rect.move({-10, -5});
  showArea(&rect);

  tchervinsky::Circle circle(5.0, {1.0, 2.0 });
  showArea(&circle);

  circle.scale(4.0);
  showArea(&circle);

  //tchervinsky::point_t polyPoints[] = {{0.0, .0}, {1.0, 1.0}, {2.0, 2.0}, {3.0, 3.0}, {4.0, 4.0}};
#ifdef POINTS_REFS
  // constructor with references to the points
  tchervinsky::point_t p0({ 1.0, 4.0 });
  tchervinsky::point_t p1({ 3.0, 2.0 });
  tchervinsky::point_t p2({ 5.0, 3.0 });
  tchervinsky::point_t p3({ 6.0, 5.0 });
  tchervinsky::point_t p4({ 3.0, 6.0 });
  tchervinsky::point_t *polyPointsRef[] = { &p0, &p1, &p2, &p3, &p4 };
  size_t numPointsRef = sizeof(polyPointsRef) / sizeof(polyPointsRef[0]);
  tchervinsky::Polygon polygon(numPointsRef, polyPointsRef);
#else
  tchervinsky::point_t polyPoints[] = {{4.0, 1.0}, {7.0, 1.0}, {11.0, 3.0}, {6.0, 8.0}, {2.0, 4.0}};
  size_t numPoints = sizeof(polyPoints) / sizeof(polyPoints[0]);
  tchervinsky::Polygon polygon(numPoints, polyPoints);
#endif // POINTS_REFS
  polygon.printInfo();

  std::cout << "Polygon scale 1" << std::endl;
  polygon.scale(4.0);
  polygon.printInfo();

  std::cout << "Polygon scale 2" << std::endl;
  polygon.scale(0.25);
  polygon.printInfo();

  std::cout << "move dx dy" << std::endl;
  polygon.move(-1, 3);
  polygon.printInfo();

  std::cout << "move to point" << std::endl;
  const tchervinsky::point_t point({ 0, 0 });
  polygon.move(point);
  polygon.printInfo();

  std::cout << "Demo polymorphism on calculation area" << std::endl;
  showArea(&polygon);

  std::cout << "Demo copy constructor of polygon" << std::endl;
  tchervinsky::Polygon polygonCopyConstructor(polygon);
  polygonCopyConstructor.printInfo();

  std::cout << "Demo copy assignment of polygon" << std::endl;
  tchervinsky::Polygon polygonCopyAssignment;
  polygonCopyAssignment = polygon;
  polygonCopyAssignment.printInfo();
  std::cout << "Is source polygon alive ?" << std::endl;
  polygon.printInfo();

  std::cout << "Demo move constructor of polygon" << std::endl;
  tchervinsky::Polygon polygonMoveConstructor(std::move(polygonCopyAssignment));
  polygonMoveConstructor.printInfo();

  std::cout << "Demo move assignment of polygon" << std::endl;
  tchervinsky::Polygon polygonMoveAssignment;
  polygonMoveAssignment = (std::move(polygonCopyConstructor));
  polygonMoveAssignment.printInfo();
  std::cout << "Is moved polygon alive ?" << std::endl;
  polygonCopyConstructor.printInfo();

  std::cout << "Try to delete nullptr" << std::endl;
  int *ptr = new int [1];
  delete [] ptr;
  ptr = nullptr;
  delete [] ptr;
  std::cout << "Succeess" << std::endl;

  return 0;
}

