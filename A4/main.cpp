#include <iostream>
#include <memory>
#include <cassert>
#include <cstddef>

#include "rectangle.hpp"
#include "circle.hpp"
#include "polygon.hpp"
#include "composite-shape.hpp"


struct Node {
  int data;
  std::shared_ptr<Node> next;
  Node(int data) :
    data{data},
    next{nullptr}
  {}
  ~Node()
  {
    std::cout << __FUNCTION__ << " data: " << data << std::endl;
  }
};

class A
{
public:
  A()
  {
    std::cout << "OOPS!!! A constructor is called" << std::endl;
  }
};

// matrix.hpp
namespace tchervinsky
{
  class Matrix
  {
  public:
    Matrix();
    Matrix(const Matrix & other) = default;
    Matrix(Matrix && other) = default;
    ~Matrix() = default;
    Matrix & operator =(const Matrix & other) = default;
    Matrix & operator =(Matrix && other) = default;
    void printInfo() const;
  #if 0
    ~Matrix() = default;
  #endif
    //void addShape(Shape * shape);
#if 0
    void addShape(std::shared_ptr<tchervinsky::Shape>shape);
#else
    void addShape(tchervinsky::Shape * shape);
#endif

  private:
    std::size_t count_;
    //std::shared_ptr<A> matrix_;
    //std::shared_ptr<A*> matrix_;

//    std::shared_ptr<A**> matrix_;
    std::shared_ptr<Shape> * matrix_;
    //std::unique_ptr<Shape *[]> figures_;
#if 1 // shared
    std::shared_ptr<tchervinsky::Shape *[]> shapes_;
    //std::shared_ptr<std::shared_ptr<tchervinsky::Shape>[]> shapes_;
#else // unique deleted copy constructor
    std::unique_ptr<tchervinsky::Shape *[]> shapes_;
    //std::unique_ptr<std::shared_ptr<tchervinsky::Shape>[]> shapes_;
#endif
    //std::shared_ptr<Shape> matrix_[]; // error: Flexible array member
//    std::shared_ptr<A*> matrix_[];

    //std::shared_ptr<tchervinsky::Shape> matrix_;
    //std::shared_ptr<Shape**> matrix_;
//  std::shared_ptr<tchervinsky::Shape *[]> s3;
  };
} // namespace
// end of matrix.hpp

// matrix.cpp
tchervinsky::Matrix::Matrix() :
  count_(0),
  matrix_(nullptr),
  //figures_(nullptr),
  //figures_(new tchervinsky::Shape * [1]),
#if 1
  shapes_(std::shared_ptr<tchervinsky::Shape *[]>(new tchervinsky::Shape *[1], [] (tchervinsky::Shape **i) { delete[] i; }))
  //shapes_(std::make_shared(1))
  //shapes_(std::make_shared<tchervinsky::Shape *[]>(1))
  //shapes_(std::make_shared<tchervinsky::Shape *[]>(1))

  //shapes_(std::make_shared(new tchervinsky::Shape *[1]))
  //shapes_(new tchervinsky::Shape *[](1))

  //shapes_(std::make_shared<tchervinsky::Shape*[]>(1))
#else
  shapes_(std::make_unique<tchervinsky::Shape*[]>(1))
#endif
//  shapes_(std::shared_ptr<new std::shared_ptr<tchervinsky::Shape>>[1])
//  shapes_(std::make_shared<new std::shared_ptr<tchervinsky::Shape>>[1])
//  shapes_(new tchervinsky::Shape * [1])
//  shapes_(new <std::shared_ptr<tchervinsky::Shape>[1]>)
{
  //std::shared_ptr<int[]> a1 = std::make_shared<int[]>(5);

//  s3 = std::make_shared<tchervinsky::Shape *[]>(9);

//  shapes_ = std::make_shared<tchervinsky::Shape *[]>(9);
//  shapes_ = std::make_shared<tchervinsky::Shape*[]>(1);

}
#if 0
tchervinsky::Matrix::~Matrix()
{
  delete [] matrix_;
  delete figures_[0];
}
#endif
#if 0
void tchervinsky::Matrix::addShape(std::shared_ptr<tchervinsky::Shape>shape)
#else
void tchervinsky::Matrix::addShape(tchervinsky::Shape * shape)
#endif
//void tchervinsky::Matrix::addShape(tchervinsky::Shape * shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("For Matrix addShape shape argument is nullptr");
  }

  count_++;
  //matrix_ = new std::shared_ptr<tchervinsky::Shape>[count_];
  //matrix_[0] = shape;

//  std::unique_ptr<Shape *[]> tmp (new Shape *[count_+1]);
//  std::swap(figures_, tmp);
  //figures_ = new tchervinsky::Shape * [count_];
  //figures_[0] = new tchervinsky::Circle(1.0, { 1.0, 1.0 });

  //shapes_[0] = shape;

  //auto n0{std::make_unique<Node>(5)};
  //std::shared_ptr<A> smartptr(new A[1], std::default_delete<A[]>());

  //std::shared_ptr<A*> smartptr(new A*[1]);
#if 0
  std::shared_ptr<A*> smartptr(new A*[1]);
  matrix_ = smartptr;
#else
  //std::shared_ptr<tchervinsky::Shape> smartptr = std::make_unique<tchervinsky::Circle *>(shape);
  //matrix_ = std::make_unique<A *>(new A  [100]);
  //matrix_ = std::make_shared<A **>(new A * [10]);
#endif

  //std::shared_ptr<A*> smartptr(new A*[1], std::default_delete<A*[]>());
  //  auto n0{std::make_unique<tchervinsky::Circle>(5.0, { 1.0, 2.0 })};
  //matrix_ = std::make_unique<tchervinsky::Circle>(5.0, { 1.0, 2.0 });
  //matrix_ = std::make_unique(tchervinsky::Shape * );
  //matrix_ = std::make_unique(tchervinsky::Shape * [count_ + 1]);
}
// end of matrix.cpp

void tchervinsky::Matrix::printInfo() const
{
  std::cout << "Matrix" << std::endl;
  std::cout << "count_: " << count_ << std::endl;
}

void showRectangle(tchervinsky::rectangle_t rect)
{
  std::cout << "width: " << rect.width << ", height: " << rect.height;
  std::cout << ", x: " << rect.pos.x << ", y: " << rect.pos.y;
  std::cout << std::endl;
}

void showArea(const tchervinsky::Shape *shape)
{
  assert(shape != nullptr && "shape parameter is invalid, nullptr");
  std::cout << "Area: " << shape->getArea() << std::endl;
}

#if 1
//static std::shared_ptr<int[]> a_global;
void how_do_shared()
{
//  a_global = std::make_shared<int[]>(5);
  std::shared_ptr<tchervinsky::Shape *[]> s4;
//  s4 = new tchervinsky::Shape *[][9];
  s4 = std::shared_ptr<tchervinsky::Shape *[]>(new tchervinsky::Shape *[1], [] (tchervinsky::Shape **i) { delete[] i; });

  std::shared_ptr<int> sp(new int[10]);
  //std::shared_ptr<int> sp(new int[10], [](int *p) { delete[] p; });

}
#endif

int main()
{
  tchervinsky::Rectangle rect(10.0, 5.0, {15.0, 15.0});
  showArea(&rect);
  rect.move({-10, -5});
  showArea(&rect);

  tchervinsky::Circle circle(5.0, {1.0, 2.0 });
  showArea(&circle);

  circle.scale(4.0);
  showArea(&circle);

  //tchervinsky::point_t polyPoints[] = {{0.0, .0}, {1.0, 1.0}, {2.0, 2.0}, {3.0, 3.0}, {4.0, 4.0}};
#ifdef POINTS_REFS
  // constructor with references to the points
  tchervinsky::point_t p0({ 1.0, 4.0 });
  tchervinsky::point_t p1({ 3.0, 2.0 });
  tchervinsky::point_t p2({ 5.0, 3.0 });
  tchervinsky::point_t p3({ 6.0, 5.0 });
  tchervinsky::point_t p4({ 3.0, 6.0 });
  tchervinsky::point_t *polyPointsRef[] = { &p0, &p1, &p2, &p3, &p4 };
  size_t numPointsRef = sizeof(polyPointsRef) / sizeof(polyPointsRef[0]);
  tchervinsky::Polygon polygon(numPointsRef, polyPointsRef);
#else
  tchervinsky::point_t polyPoints[] = {{4.0, 1.0}, {7.0, 1.0}, {11.0, 3.0}, {6.0, 8.0}, {2.0, 4.0}};
  size_t numPoints = sizeof(polyPoints) / sizeof(polyPoints[0]);
  tchervinsky::Polygon polygon(numPoints, polyPoints);
#endif // POINTS_REFS
  polygon.printInfo();

  std::cout << "Polygon scale 1" << std::endl;
  polygon.scale(4.0);
  polygon.printInfo();

  std::cout << "Polygon scale 2" << std::endl;
  polygon.scale(0.25);
  polygon.printInfo();

  std::cout << "move dx dy" << std::endl;
  polygon.move(-1, 3);
  polygon.printInfo();

  std::cout << "move to point" << std::endl;
  const tchervinsky::point_t point({ 0, 0 });
  polygon.move(point);
  polygon.printInfo();

  std::cout << "Demo polymorphism on calculation area" << std::endl;
  showArea(&polygon);

  std::cout << "Demo copy constructor of polygon" << std::endl;
  tchervinsky::Polygon polygonCopyConstructor(polygon);
  polygonCopyConstructor.printInfo();

  std::cout << "Demo copy assignment of polygon" << std::endl;
  tchervinsky::Polygon polygonCopyAssignment;
  polygonCopyAssignment = polygon;
  polygonCopyAssignment.printInfo();
  std::cout << "Is source polygon alive ?" << std::endl;
  polygon.printInfo();

  std::cout << "Demo move constructor of polygon" << std::endl;
  tchervinsky::Polygon polygonMoveConstructor(std::move(polygonCopyAssignment));
  polygonMoveConstructor.printInfo();

  std::cout << "Demo move assignment of polygon" << std::endl;
  tchervinsky::Polygon polygonMoveAssignment;
  polygonMoveAssignment = (std::move(polygonCopyConstructor));
  polygonMoveAssignment.printInfo();
  std::cout << "Is moved polygon alive ?" << std::endl;
  polygonCopyConstructor.printInfo();

  std::cout << "Try to delete nullptr" << std::endl;
  int *ptr = new int [1];
  delete [] ptr;
  ptr = nullptr;
  delete [] ptr;
  std::cout << "Succeess" << std::endl;

  std::cout << "Demo of composite shape" << std::endl;
  tchervinsky::CompositeShape compShape;
  tchervinsky::Rectangle rect4Comp(10, 10, { 0, 0 });
  compShape.addShape(&rect4Comp);
  tchervinsky::Circle circle4Comp(5.0, {1.0, 2.0 });
  compShape.addShape(&circle4Comp);
  compShape.removeShape(1);
  compShape.removeShape(0);
#if 1
  const tchervinsky::point_t circ4matrix_center = { 1.0, 1.0 };
  auto circ4matrix = tchervinsky::Circle( 1.0, circ4matrix_center);
  //auto circ4matrix = std::make_shared<tchervinsky::Circle>( 1.0, circ4matrix_center);
  //auto circ4matrix = std::make_shared<tchervinsky::Circle>( 1.0, { 1.0, 1.0 });
  //std::shared_ptr<tchervinsky::Circle>circ4matrix = std::make_shared<tchervinsky::Circle>( 1.0, { 1.0, 1.0 });
  //tchervinsky::Circle * circ4matrix = new tchervinsky::Circle( 1.0, { 1.0, 1.0 });
  std::cout << "Matrix demo" << std::endl;
  tchervinsky::Matrix matrix;
  matrix.addShape(&circ4matrix);
#if 0
  // copy constructor
  std::cout << "Demo copy constructor of Matrix" << std::endl;
  tchervinsky::Matrix matricCopyByConstructor(matrix);
  CopyByConstructor.printInfo();
#endif
#else
  tchervinsky::Circle circ4matrix( 1.0, { 1.0, 1.0 });
  std::cout << "Matrix demo" << std::endl;
  tchervinsky::Matrix matrix;
  matrix.addShape(std::make_shared<tchervinsky::Circle>(circ4matrix));

  std::shared_ptr<int[]> a1 = std::make_shared<int[]>(5);
  std::shared_ptr<int[]> a2 = std::shared_ptr<int[]>(new int[2]);
  std::shared_ptr<tchervinsky::Shape[]> s1 = std::make_shared<tchervinsky::Shape[]>(3);
  std::shared_ptr<tchervinsky::Shape *[]> s2 = std::make_shared<tchervinsky::Shape *[]>(7);
  std::shared_ptr<tchervinsky::Shape *[]> s3;
  s3 = std::make_shared<tchervinsky::Shape *[]>(9);
  std::shared_ptr<tchervinsky::Shape *[]> s4;
  s4 = std::shared_ptr<tchervinsky::Shape *[]>(new tchervinsky::Shape *[](9));

  //boost::shared_ptr<int[]> a1 = boost::make_shared<int[]>(size);
#endif
  return 0;
}
