#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP

#include "shape.hpp"

// center: https://www.mathopenref.com/coordcentroid.html
// area: https://math.stackexchange.com/questions/516219/finding-out-the-area-of-a-triangle-if-the-coordinates-of-the-three-vertices-are
// scale: https://math.stackexchange.com/questions/1563249/how-do-i-scale-a-triangle-given-its-cartesian-cooordinates

namespace tchervinsky
{

  class Triangle: public Shape
  {
  public:
  Triangle(const point_t & point1, const point_t & point2, const point_t & point3);
  double getArea() const override;
  rectangle_t getFrameRect() const override;
  void move(const point_t & pos) override; // смещение в заданную точку
  void move(double x, double y) override; // смещение по осям
  void scale(double coefficient) override;
  void showTriangle() const;

  private:
  point_t p1_;
  point_t p2_;
  point_t p3_;
  point_t center_;
  };

}
#endif
