#include "composite-shape.hpp"
#include <stdexcept>
#include <algorithm>

tchervinsky::CompositeShape::CompositeShape():
  count_(0),
  shapes_(nullptr),
  center_({ 0, 0 })
{
}

tchervinsky::CompositeShape::CompositeShape(const CompositeShape & other)
{}

tchervinsky::CompositeShape::CompositeShape(CompositeShape && other)
{}

tchervinsky::CompositeShape::~CompositeShape()
{
  delete [] shapes_;
}

tchervinsky::CompositeShape & tchervinsky::CompositeShape::operator =(const CompositeShape & other)
{
  return *this; // TODO: change
}

tchervinsky::CompositeShape & tchervinsky::CompositeShape::operator =(CompositeShape && other)
{
  return *this; // TODO: change
}

double tchervinsky::CompositeShape::getArea() const
{
  return 0;
}

tchervinsky::rectangle_t tchervinsky::CompositeShape::getFrameRect() const
{
  if (count_ == 0)
  {
    throw std::logic_error("For Composite Shape, no figures to calculate frame rect");
  }

  tchervinsky::rectangle_t rect = shapes_[0]->getFrameRect();
  double minX = rect.pos.x - rect.width / 2;
  double maxX = rect.pos.x + rect.width / 2;
  double minY = rect.pos.y - rect.height / 2;
  double maxY = rect.pos.y + rect.height / 2;

  for (std::size_t i = 1; i < count_; i++)
  {
    rect = shapes_[i]->getFrameRect();
    minX = std::min(minX, rect.pos.x - rect.width / 2);
    maxX = std::max(maxX, rect.pos.x + rect.width / 2);
    minY = std::min(minY, rect.pos.y - rect.height / 2);
    maxY = std::max(maxY, rect.pos.y + rect.height / 2);
  }

  const double width = maxX - minX;
  const double height = maxY - minY;

  return { width, height, { minX + width / 2, minY + height / 2 } };
}

void tchervinsky::CompositeShape::move(const tchervinsky::point_t & pos)
{
  tchervinsky::rectangle_t rect = getFrameRect();
  center_.x = rect.pos.x;
  center_.y = rect.pos.y;

  const double dx = center_.x - pos.x;
  const double dy = center_.y - pos.y;

  move(dx, dy);
}

void tchervinsky::CompositeShape::move(double dx, double dy)
{
  for (std::size_t i = 0; i < count_; ++i)
  {
    shapes_[i]->move(dx, dy);
  }
}

void tchervinsky::CompositeShape::scale(double coefficient)
{
}

void tchervinsky::CompositeShape::addShape(tchervinsky::CompositeShape::Shape * shape)
{
  if(shape == nullptr)
  {
    throw std::invalid_argument("Composite shape addShape shape argument is nullptr");
  }
  // check existence
  for (std::size_t i = 0; i < count_; ++i)
  {
    if (shapes_[i] == shape)
    {
      throw std::invalid_argument("Composite shape addShape shape exists in the composite, duplicates are not allowed");
    }
  }
  // allocate memory
  tchervinsky::Shape ** tmpShapes = new tchervinsky::Shape * [count_ + 1];
  // copy existed array
  for (std::size_t i = 0; i < count_; ++i)
  {
    tmpShapes[i] = shapes_[i];
  }
  // add new
  tmpShapes[count_] = shape;
  // delete current array
  delete [] shapes_;
  // set tmp as Current
  shapes_ = tmpShapes;
  // adjust counter
  ++count_;
}

void tchervinsky::CompositeShape::removeShape(size_t index)
{
  if (index >= count_)
  {
    throw std::invalid_argument("Composite shape removeShape index is greater than number of shapes in the composite");
  }
  // allocate temporary array
  tchervinsky::Shape ** tmpShapes = new tchervinsky::Shape * [count_ - 1];
  // copy skipping removed
  for (size_t i = 0; (i < count_) && (i != index); ++i)
  {
    tmpShapes[i] = shapes_[i];
  }
  // free current
  delete [] shapes_;
  // set Current
  shapes_ = tmpShapes;
  // adjust counter
  --count_;
}
