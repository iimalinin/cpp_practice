#include "triangle.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>

tchervinsky::Triangle::Triangle(const tchervinsky::point_t & point1, const tchervinsky::point_t & point2,
    const tchervinsky::point_t & point3):
  p1_(point1),
  p2_(point2),
  p3_(point3),
  center_({ (p1_.x + p2_.x + p3_.x) / 3, (p1_.y + p2_.y + p3_.y) / 3 })
{
  if (getArea() == 0)
  {
    throw std::invalid_argument("The area of triangle is zero, it means the points are collinear");
  }
}

double tchervinsky::Triangle::getArea() const
{
  return (std::abs((p1_.x * (p2_.y - p3_.y)) + (p2_.x * (p3_.y - p1_.y)) + (p3_.x * (p1_.y - p2_.y)))) / 2;
}

tchervinsky::rectangle_t tchervinsky::Triangle::getFrameRect() const
{
  const double min_x = std::min(std::min(p1_.x, p2_.x), p3_.x);
  const double max_x = std::max(std::max(p1_.x, p2_.x), p3_.x);
  const double min_y = std::min(std::min(p1_.y, p2_.y), p3_.y);
  const double max_y = std::max(std::max(p1_.y, p2_.y), p3_.y);

  tchervinsky::rectangle_t rect;
  rect.width = max_x - min_x;
  rect.height = max_y - min_y;
  rect.pos.x = min_x + (rect.width / 2);
  rect.pos.y = min_y + (rect.height / 2);

  return rect;
}

void tchervinsky::Triangle::move(const point_t & pos)
{
  double dx = pos.x - p1_.x;
  double dy = pos.y - p1_.y;

  move(dx, dy);
}

void tchervinsky::Triangle::move(double dx, double dy)
{
  p1_.x += dx;
  p1_.y += dy;

  p2_.x += dx;
  p2_.y += dy;

  p3_.x += dx;
  p3_.y += dy;

  center_.x += dx;
  center_.y += dy;
#if 0
  rect_.x += dx;
  rect_.y += dy;
#endif
}

void tchervinsky::Triangle::scale(double coefficient)
{
  if (coefficient <= 0)
  {
    throw std::invalid_argument("For triangle, error: scale coefficient must be positive.");
  }
  p1_ = { p1_.x * coefficient - center_.x, p1_.y * coefficient - center_.y };
  p2_ = { p2_.x * coefficient - center_.x, p2_.y * coefficient - center_.y };
  p3_ = { p3_.x * coefficient - center_.x, p3_.y * coefficient - center_.y };
}

void tchervinsky::Triangle::showTriangle() const
{
  std::cout << "Triangle: " << std::endl;
  std::cout << "point 1: " << "x: " << p1_.x << " y: " << p1_.y << std::endl;
  std::cout << "point 2: " << "x: " << p2_.x << " y: " << p2_.y << std::endl;
  std::cout << "point 3: " << "x: " << p3_.x << " y: " << p3_.y << std::endl;
  std::cout << "center:  " << "x: " << center_.x << " y: " << center_.y << std::endl;
}
