#include "polygon.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>
#include <algorithm>


// default constructor
tchervinsky::Polygon::Polygon():
  numPoints_(0),
  center_({ 0, 0 }),
  points_(nullptr)
{
}

#ifdef POINTS_REFS
// constructor with parameters, points by reference
tchervinsky::Polygon::Polygon(const std::size_t numPoints, tchervinsky::point_t **polyPoints):
  numPoints_(numPoints)
{
  if (polyPoints == nullptr)
  {
    throw std::invalid_argument("For polygon, error: pointer to array of points is null");
  }

  if (numPoints_ < 3)
  {
    throw std::invalid_argument("For polygon, error: at least 3 vertex must be given.");
  }

  points_ = new tchervinsky::point_t *[numPoints_];
  for (std::size_t i = 0; i < numPoints_; ++i)
  {
      points_[i] = polyPoints[i];
  }

  if (getArea() == 0)
  {
    throw std::invalid_argument("For polygon, error: the vertexes are collinear.");
  }

  center_ = calcCenroid();
}

#else
// constructor with parameters, points by value
tchervinsky::Polygon::Polygon(const std::size_t numPoints, const tchervinsky::point_t *polyPoints):
  numPoints_(numPoints)
{
  if (polyPoints == nullptr)
  {
    throw std::invalid_argument("For polygon, error: pointer to array of points is null");
  }

  if (numPoints_ < 3)
  {
    throw std::invalid_argument("For polygon, error: at least 3 vertex must be given.");
  }

  points_ = new tchervinsky::point_t[numPoints_];
  //points_ = new tchervinsky::point_t[numPoints_ * (50000000000000ul)]; // bad_alloc
  std::cout << __LINE__ << std::endl;
  for (std::size_t i = 0; i < numPoints_; ++i)
  {
    points_[i] = polyPoints[i];
  }

  if (getArea() == 0)
  {
    throw std::invalid_argument("For polygon, error: the vertexes are collinear.");
  }

  center_ = calcCenroid();
}
#endif

// copy constructor
#ifdef POINTS_REFS
tchervinsky::Polygon::Polygon(const tchervinsky::Polygon & other):
  numPoints_(other.numPoints_),
  center_(other.center_),
  points_(new tchervinsky::point_t *[numPoints_])

{
  for (std::size_t i = 0; i < numPoints_; ++i)
  {
      points_[i] = other.points_[i];
  }
}
#else
tchervinsky::Polygon::Polygon(const tchervinsky::Polygon & other):
  numPoints_(other.numPoints_),
  center_(other.center_),
  points_(new tchervinsky::point_t [numPoints_])

{
  for (std::size_t i = 0; i < numPoints_; ++i)
  {
      points_[i] = other.points_[i];
  }
}
#endif

// move constructor
tchervinsky::Polygon::Polygon(tchervinsky::Polygon && other):
  numPoints_(other.numPoints_),
  center_(other.center_)
{
  other.numPoints_ = 0;
  other.center_ = { 0, 0 };

  points_ = other.points_;
  other.points_ = nullptr;
}

// destructor
tchervinsky::Polygon::~Polygon()
{
  delete [] points_;
}

// copy assignment
#ifdef POINTS_REFS
tchervinsky::Polygon & tchervinsky::Polygon::operator =(const Polygon & other)
{
  if (this == &other)
  {
    return *this;
  }

  numPoints_ = other.numPoints_;
  center_ = other.center_;

  delete [] points_;

  points_ = new tchervinsky::point_t *[numPoints_];
  for (std::size_t i = 0; i < numPoints_; ++i)
  {
      points_[i] = other.points_[i];
  }
  return *this;
};
#else
tchervinsky::Polygon & tchervinsky::Polygon::operator =(const Polygon & other)
{
  if (this == &other)
  {
    return *this;
  }

  numPoints_ = other.numPoints_;
  center_ = other.center_;

  delete [] points_;

  points_ = new tchervinsky::point_t [numPoints_];
  for (std::size_t i = 0; i < numPoints_; ++i)
  {
      points_[i] = other.points_[i];
  }
  return *this;
};
#endif

// move assignment
tchervinsky::Polygon & tchervinsky::Polygon::operator =(Polygon && other)
{
  if (this == &other)
  {
    return *this;
  }

  numPoints_ = other.numPoints_;
  center_ = other.center_;

  other.numPoints_ = 0;
  other.center_ = { 0, 0 };

  delete [] points_;
  points_ = other.points_;
  other.points_ = nullptr;
  return *this;
};

/* https://stackoverflow.com/questions/16285134/calculating-polygon-area
JavaScript
function calcPolygonArea(vertices) {
    var total = 0;

    for (var i = 0, l = vertices.length; i < l; i++) {
      var addX = vertices[i].x;
      var addY = vertices[i == vertices.length - 1 ? 0 : i + 1].y;
      var subX = vertices[i == vertices.length - 1 ? 0 : i + 1].x;
      var subY = vertices[i].y;

      total += (addX * addY * 0.5);
      total -= (subX * subY * 0.5);
    }

    return Math.abs(total);
}
*/

// daniil demchuk. incorrect
#if 0
double tchervinsky::Polygon::getArea() const
{
  double sum = 0;
  for (int i = 0; i < count_ - 1; i++)
  {
    sum = sum + (vertices_[i].x + vertices_[i + 1].x) * (vertices_[i].y - vertices_[i + 1].y);
  }
  sum = sum + (vertices_[0].x + vertices_[count_ - 1].x) * (vertices_[0].y + vertices_[count_ - 1].y);
  return sum / 2;
}
// end of daniil demchuk
#endif
double tchervinsky::Polygon::getArea() const
{
  double area = 0;
#ifdef POINTS_REFS

  for (std::size_t i = 0; i < numPoints_; ++i)
  {
    const double addX = points_[i]->x;
    const double addY = points_[i == (numPoints_ - 1) ? 0 : (i + 1)]->y;
    const double subX = points_[i == (numPoints_ - 1) ? 0 : (i + 1)]->x;
    const double subY = points_[i]->y;

    area += (addX * addY * 0.5);
    area -= (subX * subY * 0.5);
  }

#else
  for (std::size_t i = 0; i < numPoints_; ++i)
  {
    const double addX = points_[i].x;
    const double addY = points_[i == (numPoints_ - 1) ? 0 : (i + 1)].y;
    const double subX = points_[i == (numPoints_ - 1) ? 0 : (i + 1)].x;
    const double subY = points_[i].y;

    area += (addX * addY * 0.5);
    area -= (subX * subY * 0.5);
  }
#endif
  return std::abs(area);
}

/*
  здравый смысл, обобщение треугольника
*/
tchervinsky::rectangle_t tchervinsky::Polygon::getFrameRect() const
{
  if (numPoints_ == 0)
  {
    return { 0, 0, { 0, 0} };
  }
#ifdef POINTS_REFS

  double minX = points_[0]->x;
  double maxX = points_[0]->x;
  double minY = points_[0]->y;
  double maxY = points_[0]->y;

  for (std::size_t i = 0; i < numPoints_; ++i)
  {
    minX = std::min(minX, points_[i]->x);
    maxX = std::max(maxX, points_[i]->x);
    minY = std::min(minY, points_[i]->y);
    maxY = std::max(maxY, points_[i]->y);
  }
  const double width = maxX - minX;
  const double height = maxY - minY;
  const tchervinsky::point_t center = { minX + ( width / 2 ), minY + ( height / 2 ) };
  return { width, height, center };

#else
  double minX = points_[0].x;
  double maxX = points_[0].x;
  double minY = points_[0].y;
  double maxY = points_[0].y;

  for (std::size_t i = 0; i < numPoints_; ++i)
  {
    minX = std::min(minX, points_[i].x);
    maxX = std::max(maxX, points_[i].x);
    minY = std::min(minY, points_[i].y);
    maxY = std::max(maxY, points_[i].y);
  }
  const double width = maxX - minX;
  const double height = maxY - minY;
  const tchervinsky::point_t center = { minX + ( width / 2 ), minY + ( height / 2 ) };
  return { width, height, center };
#endif
}

void tchervinsky::Polygon::move(const tchervinsky::point_t & pos)
{
  double dx = pos.x - center_.x;
  double dy = pos.y - center_.y;

  move(dx, dy);
}

void tchervinsky::Polygon::move(double dx, double dy)
{
#ifdef POINTS_REFS

  for (size_t i = 0; i < numPoints_; ++i)
  {
    points_[i]->x += dx;
    points_[i]->y += dy;
  }

#else
  for (size_t i = 0; i < numPoints_; ++i)
  {
    points_[i].x += dx;
    points_[i].y += dy;
  }
#endif
  center_.x += dx;
  center_.y += dy;
}

/*
  здравый смысл, обобщение треугольника, рисунок, расчеты
*/
void tchervinsky::Polygon::scale(const double coefficient)
{
  if (coefficient <= 0)
  {
    throw std::invalid_argument("For polygon, error: scale coefficient must be positive.");
  }

#ifdef POINTS_REFS

  for (size_t i = 0; i < numPoints_; ++i)
  {
    points_[i]->x = center_.x + (points_[i]->x - center_.x) * coefficient;
    points_[i]->y = center_.y + (points_[i]->y - center_.y) * coefficient;
  }
}

#else
  for (size_t i = 0; i < numPoints_; ++i)
  {
    points_[i].x = center_.x + (points_[i].x - center_.x) * coefficient;
    points_[i].y = center_.y + (points_[i].y - center_.y) * coefficient;
  }
}
#endif


/* https://stackoverflow.com/questions/2792443/finding-the-centroid-of-a-polygon
Point2D compute2DPolygonCentroid(const Point2D* vertices, int vertexCount)
{
    Point2D centroid = {0, 0};
    double signedArea = 0.0;
    double x0 = 0.0; // Current vertex X
    double y0 = 0.0; // Current vertex Y
    double x1 = 0.0; // Next vertex X
    double y1 = 0.0; // Next vertex Y
    double a = 0.0;  // Partial signed area

    // For all vertices
    int i=0;
    for (i=0; i<vertexCount; ++i)
    {
        x0 = vertices[i].x;
        y0 = vertices[i].y;
        x1 = vertices[(i+1) % vertexCount].x;
        y1 = vertices[(i+1) % vertexCount].y;
        a = x0*y1 - x1*y0;
        signedArea += a;
        centroid.x += (x0 + x1)*a;
        centroid.y += (y0 + y1)*a;
    }

    signedArea *= 0.5;
    centroid.x /= (6.0*signedArea);
    centroid.y /= (6.0*signedArea);

    return centroid;
}
*/
tchervinsky::point_t tchervinsky::Polygon::calcCenroid() const
{
  const double triangleNumPointsMultTwo = 6.0;

  tchervinsky::point_t centroid = {0, 0};
  double signedArea = 0.0;
  double x0 = 0.0; // Current vertex X
  double y0 = 0.0; // Current vertex Y
  double x1 = 0.0; // Next vertex X
  double y1 = 0.0; // Next vertex Y
  double a = 0.0;  // Partial signed area

  // For all vertices
#ifdef POINTS_REFS

  for (std::size_t i = 0; i < numPoints_; ++i)
  {
      x0 = points_[i]->x;
      y0 = points_[i]->y;
      x1 = points_[(i+1) % numPoints_]->x;
      y1 = points_[(i+1) % numPoints_]->y;
      a = (x0 * y1) - (x1 * y0);
      signedArea += a;
      centroid.x += (x0 + x1) * a;
      centroid.y += (y0 + y1) * a;
  }

#else
  for (std::size_t i = 0; i < numPoints_; ++i)
  {
      x0 = points_[i].x;
      y0 = points_[i].y;
      x1 = points_[(i+1) % numPoints_].x;
      y1 = points_[(i+1) % numPoints_].y;
      a = (x0 * y1) - (x1 * y0);
      signedArea += a;
      centroid.x += (x0 + x1) * a;
      centroid.y += (y0 + y1) * a;
  }
#endif

  signedArea /= 2;

  centroid.x /= (triangleNumPointsMultTwo * signedArea);
  centroid.y /= (triangleNumPointsMultTwo * signedArea);

  return centroid;
}


/* http://csharphelper.com/blog/2014/07/determine-whether-a-polygon-is-convex-in-c/
C#
// Return True if the polygon is convex.
public bool PolygonIsConvex()
{
    // For each set of three adjacent points A, B, C,
    // find the cross product AB · BC. If the sign of
    // all the cross products is the same, the angles
    // are all positive or negative (depending on the
    // order in which we visit them) so the polygon
    // is convex.
    bool gotNegative = false;
    bool got_positive = false;
    int num_points = Points.Length;
    int B, C;
    for (int A = 0; A < num_points; A++)
    {
        B = (A + 1) % num_points;
        C = (B + 1) % num_points;

        float cross_product =
            CrossProductLength(
                Points[A].X, Points[A].Y,
                Points[B].X, Points[B].Y,
                Points[C].X, Points[C].Y);
        if (cross_product < 0)
        {
            gotNegative = true;
        }
        else if (cross_product > 0)
        {
            got_positive = true;
        }
        if (gotNegative && got_positive) return false;
    }

    // If we got this far, the polygon is convex.
    return true;
}

// Return the cross product AB x BC.
// The cross product is a vector perpendicular to AB
// and BC having length |AB| * |BC| * Sin(theta) and
// with direction given by the right-hand rule.
// For two vectors in the X-Y plane, the result is a
// vector with X and Y components 0 so the Z component
// gives the vector's length and direction.
public static float CrossProductLength(float Ax, float Ay,
    float Bx, float By, float Cx, float Cy)
{
    // Get the vectors' coordinates.
    float BAx = Ax - Bx;
    float BAy = Ay - By;
    float BCx = Cx - Bx;
    float BCy = Cy - By;

    // Calculate the Z coordinate of the cross product.
    return (BAx * BCy - BAy * BCx);
}
*/
static double crossProductLength(double xA, double yA,
    double xB, double yB, double xC, double yC)
{
    // Get the vectors' coordinates.
    double xBA = xA - xB;
    double yBA = yA - yB;
    double xBC = xC - xB;
    double yBC = yC - yB;

    // Calculate the Z coordinate of the cross product.
    return (xBA * yBC - yBA * xBC);
}

bool tchervinsky::Polygon::isConvex() const
{
  bool gotNegative = false;
  bool gotPositive = false;
  for (std::size_t a = 0; a < numPoints_; ++a)
  {
    const std::size_t b = (a + 1) % numPoints_;
    const std::size_t c = (b + 1) % numPoints_;
#ifdef POINTS_REFS
    double cross_product =
      crossProductLength(
        points_[a]->x, points_[a]->y,
        points_[b]->x, points_[b]->y,
        points_[c]->x, points_[c]->y);
#else
    double cross_product =
      crossProductLength(
        points_[a].x, points_[a].y,
        points_[b].x, points_[b].y,
        points_[c].x, points_[c].y);
#endif
    if (cross_product < 0)
    {
      gotNegative = true;
    }
    else if (cross_product > 0)
    {
      gotPositive = true;
    }
    if (gotNegative && gotPositive)
    {
      return false;
    }
  }
  // If we got this far, the polygon is convex.
  return true;
}

void tchervinsky::Polygon::printInfo()
{
  std::cout << "Polygon:" << std::endl;

  if (numPoints_ == 0)
  {
    std::cout << "(empty)" << std::endl;
  }
  else
  {
    for (std::size_t i = 0; i < numPoints_; ++i)
    {
#ifdef POINTS_REFS
      std::cout << "point [" << i << "]" << " x = " << points_[i]->x << ", y = " << points_[i]->y << std::endl;
#else
      std::cout << "point [" << i << "]" << " x = " << points_[i].x << ", y = " << points_[i].y << std::endl;
#endif
    }
    std::cout << "center: " << " x = " << center_.x << ", y = " << center_.y << std::endl;
    std::cout << "Area: " << getArea() << std::endl;
    const tchervinsky::rectangle_t rect = getFrameRect();
    std::cout << "FrameRect: " << " width: " << rect.width << ", height: " << rect.height
      << ", x: " << rect.pos.x << ", y: " << rect.pos.y << std::endl;
    std::cout << "Convex: " << isConvex() << std::endl;
    }
}
