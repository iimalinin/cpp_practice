# Triangle referencees:

## center: 
<https://www.mathopenref.com/coordcentroid.html>

## area: 
<https://math.stackexchange.com/questions/516219/finding-out-the-area-of-a-triangle-if-the-coordinates-of-the-three-vertices-are>

## scale:
<https://math.stackexchange.com/questions/1563249/how-do-i-scale-a-triangle-given-its-cartesian-cooordinates>
