#ifndef SHAPE_HPP
#define SHAPE_HPP

#include "base-types.hpp"

#include <iostream> // for ~Shape output

class Shape
{
public:
/*	https://wiki.sei.cmu.edu/confluence/display/cplusplus/OOP52-CPP.+Do+not+delete+a+polymorphic+object+without+a+virtual+destructor
    Compliant Solution:
    ===================
	The destructor for Base (Shape в нашем случае) has an explicitly declared virtual destructor, 
	ensuring that the polymorphic delete operation results in well-defined behavior.
															  ^^^^^^^^^^^^^^^^^^^^^
	Risk Assessment:
	================
	Attempting to destruct a polymorphic object that does not have a virtual destructor declared 
	results in undefined behavior.
			   ^^^^^^^^^^^^^^^^^^^ 
	In practice, potential consequences include abnormal program termination and memory leaks.
*/
/*
Guideline #4: A base class destructor should be either public and virtual, or protected and nonvirtual.
Let's see why this is so.
First, an obvious statement: Clearly any operation that will be performed through the base class interface, 
and that should behave virtually, should be virtual. That's true even with Template Method, above, 
because although the public interface function is nonvirtual, the work is delegated to a 
nonpublic virtual function and we get the virtual behavior that we need.
If deletion, therefore, can be performed polymorphically through the base class interface, 
then it must behave virtually and must be virtual. 
Indeed, the language requires it - if you delete polymorphically without a virtual destructor, 
you summon the dreaded specter of "undefined behavior," a specter I personally would rather not meet 
in even a moderately well-lit alley, thank you very much.
*/
	
/* Простое объяснение Майерса - правило 33 "Делайте нетерминальные классы абстрактными":

	Хотя большинство абстрактных функций никогда не реализуются, абстрактные деструкторы
	представляют собой особый случай. Они _должны_ быть реализованы, поскольку всегда вызываются
	при вызове деструктора производного класса. Кроме того, они часто выполняют полезные действия,
	такие как высвобождение ресурсов (см. правило 9) или запись сообщений в лог-файл;
	Хотя реализация абстрактных функций в общем случае встречается довольно редко,
	дла абстрактных конструкторов это не только обычно, но и обязательно.
	
*/	
	~Shape() = default; // деструктор по умолчанию
	//virtual ~Shape() = default; // деструктор по умолчанию
	//virtual ~Shape() { std::cout << __FUNCTION__ << std::endl; }
	//~Shape() { std::cout << __FUNCTION__ << std::endl; }
	//virtual ~Shape() = 0;
	//~Shape(){};
	virtual double getArea() const = 0;
	virtual rectangle_t getFrameRect() const = 0;
	virtual void move(const point_t & pos) = 0; // смещение в заданную точку
	virtual void move(double x, double y) = 0; // смещение по осям
};

#endif // SHAPE_HPP