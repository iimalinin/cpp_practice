#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP

#include "shape.hpp"

namespace drozdov
{
  class CompositeShape:
      public Shape {
    public:
      explicit CompositeShape();
      CompositeShape(const CompositeShape &other);
      CompositeShape(CompositeShape &&other);
      CompositeShape(Shape &startShape);
      ~CompositeShape() override;

      void operator +=(Shape &newShape);
      CompositeShape& operator +(Shape &newShape);
      Shape* operator [](int index) const;
      CompositeShape& operator =(const CompositeShape &other);
      CompositeShape& operator =(CompositeShape &&other);

      void addShape(Shape &newShape);
      void removeShape(int indexRemoveShape);
      int getQuantity() const;

      void rotate(double) override;
      void printData() const override;
      double getArea() const override;
      point_t getPos() const override;
      rectangle_t getFrameRect() const override;
      void scale(double coefficient) override;
      void move(double dx, double dy) override;
      void move(const point_t &newPos) override;

    private:
      int qty_; // quantity
      Shape **shapes_;
      bool exists(const Shape *shape) const;
  };
}
#endif
