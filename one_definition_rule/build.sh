#!/bin/bash

echo Компилируем без ошибки
g++ -o main -std=c++14 -Wall -Wextra -Wold-style-cast main.cpp
echo
echo Делаем копию заголовочного файла
cp header.hpp header.bak.hpp
echo
echo Копируем в заголовочный файл файл без guard 
cp header.without.guard.hpp header.hpp
echo
echo Компилируем и видим ощибку
g++ -o main -std=c++14 -Wall -Wextra -Wold-style-cast main.cpp
echo pragma once вместо header guard описана здесь: https://ru.wikipedia.org/wiki/Pragma_once
echo Возвращаем заголовочный файл с защитой
cp header.bak.hpp header.hpp
echo
echo Компилируем без ошибки
g++ -o main -std=c++14 -Wall -Wextra -Wold-style-cast main.cpp



